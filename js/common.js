$(document).ready(function() {

    //Анимация на кнопке
    $(".toggle_mnu, .menu_item").click(function() {
        $(".sandwich").toggleClass("active");
    });
    //подбор размера главного экрана
    function heightDetect() {
        $(".main_head").css("height", $(window).height());
    };
    heightDetect()
    $(window).resize(function() {
        heightDetect()
    });
    // скрыть меню
    	$(".top_mnu ul a").click(function() {
    		$(".top_mnu").fadeOut(600);
    		$(".sandwich").toggleClass("active");
    		$(".top_text").css("opacity", "1");
    	}).append("<span>");
    //анимация появления меню
    $(".toggle_mnu").click(function() {
    	if ($(".top_mnu").is(":visible")){
    		$(".top_text").removeClass("h-opasifi");
    		$(".top_mnu").fadeOut(600);
    		$(".top_mnu").removClass("pulse animated");
    		}else{
    		$(".top_text").addClass("h-opasifi");
    		$(".top_mnu").fadeIn(600);
    		$(".top_mnu").addClass("pulse animated");
    		};
    });
    //Таймер обратного отсчета
    //Документация: http://keith-wood.name/countdown.html
    //<div class="countdown" date-time="2015-01-07"></div>
    var austDay = new Date($(".countdown").attr("date-time"));
    $(".countdown").countdown({
        until: austDay,
        format: 'yowdHMS'
    });

    //Попап менеджер FancyBox
    //Документация: http://fancybox.net/howto
    //<a class="fancybox"><img src="image.jpg" /></a>
    //<a class="fancybox" data-fancybox-group="group"><img src="image.jpg" /></a>
    $(".fancybox").fancybox();

    //Навигация по Landing Page
    //$(".top_mnu") - это верхняя панель со ссылками.
    //Ссылки вида <a href="#contacts">Контакты</a>
    $(".top_mnu").navigation();

    //Добавляет классы дочерним блокам .block для анимации
    //Документация: http://imakewebthings.com/jquery-waypoints/
    $(".block").waypoint(function(direction) {
        if (direction === "down") {
            $(".class").addClass("active");
        } else if (direction === "up") {
            $(".class").removeClass("deactive");
        };
    }, {
        offset: 100
    });

    //Плавный скролл до блока .div по клику на .scroll
    //Документация: https://github.com/flesler/jquery.scrollTo
    $("a.scroll").click(function() {
        $.scrollTo($(".div"), 1000, {
            offset: -90
        });
    });

    //Каруселька
    //Документация: http://owlgraphic.com/owlcarousel/
    var owl = $(".carousel");
    owl.owlCarousel({
        items: 4
    });
    owl.on("mousewheel", ".owl-wrapper", function(e) {
        if (e.deltaY > 0) {
            owl.trigger("owl.prev");
        } else {
            owl.trigger("owl.next");
        }
        e.preventDefault();
    });
    $(".next_button").click(function() {
        owl.trigger("owl.next");
    });
    $(".prev_button").click(function() {
        owl.trigger("owl.prev");
    });

    //Кнопка "Наверх"
    //Документация:
    //http://api.jquery.com/scrolltop/
    //http://api.jquery.com/animate/
    $("#top").click(function() {
        $("body, html").animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    //Аякс отправка форм
    //Документация: http://api.jquery.com/jquery.ajax/
    $("form").submit(function() {
        $.ajax({
            type: "GET",
            url: "mail.php",
            data: $("form").serialize()
        }).done(function() {
            alert("Спасибо за заявку!");
            setTimeout(function() {
                $.fancybox.close();
            }, 1000);
        });
        return false;
    });
    //vertical slider
    //vertical slider
   $(function() {
   $("#slider-vertical").slider({
       orientation: "vertical",
       range: "min",
       min: 0,
       max: 100,
       value: 75,
       slide: function(event, ui) {
           $("#amount").val(ui.value);
           if (ui.value>50) {
             $(".parallax-mirror:nth-child(2) img.parallax-slider").attr("src", "img/tube_vasil_little.jpg");
             $(".text_second").removeClass("hidden");
             $(".text_first").addClass("hidden");

           }
           if (ui.value<50) {
             $(".parallax-mirror:nth-child(2) img.parallax-slider").attr("src", "img/klyonky_bridge.jpg");
             $(".text_first").removeClass("hidden");
             $(".text_second").addClass("hidden");
           }
       }
   });
   $("#amount").val($("#slider-vertical").slider("value"));
});



});
