<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ru" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html lang="ru" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="ru" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="ru">
<!--<![endif]-->

<head>
	<meta charset="utf-8" />
	<title>Rope Jump Gomel</title>
	<meta name="description" content="" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="shortcut icon" href="img/rjg.ico" />
	<link rel="stylesheet" href="libs/bootstrap/bootstrap-grid-3.3.1.min.css" />
	<link rel="stylesheet" href="libs/font-awesome-4.2.0/css/font-awesome.min.css" />
	<link rel="stylesheet" href="libs/fancybox/jquery.fancybox.css" />
	<link rel="stylesheet" href="libs/owl-carousel/owl.carousel.css" />
	<link rel="stylesheet" href="libs/countdown/jquery.countdown.css" />
	<link rel="stylesheet" href="css/fonts.css" />
	<link rel="stylesheet" href="css/main.css" />
	<link rel="stylesheet" href="css/media.css" />
	<link rel="stylesheet" href="css/jquery-ui.min.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>

<body>
		<div class="div_gradient" data-z-index="2">
	<header class="main_head main_color_bg left_gradient" data-parallax="scroll" data-image-src="img/main_background.png" data-z-index="-1" id="about">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<img src="img/logo.png" alt="logotype">
				</div>
				<button class="toggle_mnu">
					<span class="sandwich">
					<span class="sw-topper"></span>
					<span class="sw-bottom"></span>
					<span class="sw-footer"></span>
					</span>
				</button>
				<nav class="top_mnu">
					<ul>
						<li><a href="#about">Главная</a></li>
						<li><a href="#why_we_are">Кого выбрать?</a></li>
						<li><a href="#our_places">Место встречи</a></li>
						<li><a href="#feed_back">Приём! Как слышно?!</a></li>
					</ul>
				</nav>
			</div>
		</div>


	</header>
</div>
	<aside class="between">

	</aside>
		<div class="div_gradient" data-z-index="2">
	<header class="main_head main_color_bg" data-parallax="scroll" data-image-src="img/HeBzo-smx6E.jpg" data-z-index="-1">
		<div class="container">
			<div class="row">
				<h1 class="chose_us">Выбирайте НАС!</h1>
				<p class="chose_us_p">
					Ведь на то есть причины
				</p>
				<div class="grid">
					<figure class="effect-ming">
						<img src="img/30.jpg" alt="img30" />
						<figcaption>
							<h2><span>Опыт</span></h2>
							<p>7 лет в сфере Rope Jump</p>
						</figcaption>
					</figure>
					<figure class="effect-ming">
						<img src="img/31.jpg" alt="img31" />
						<figcaption>
							<h2><span>Экипировка</span></h2>
							<p>Лучшее снаряжение в Беларусии</p>
						</figcaption>
					</figure>
					<figure class="effect-ming">
						<img src="img/32.jpg" alt="img32" />
						<figcaption>
							<h2><span>Гарантия</span></h2>
							<p>100% безопасности
								<br> 100% ощущений</p>
						</figcaption>
					</figure>

				</div>

			</div>
		</div>


	</header>
</div>
	<aside class="between">

	</aside>
	<div class="div_gradient" data-z-index="2">
		<header class="main_head main_color_bg"  data-parallax="scroll" data-image-src="img/tube_vasil_little.jpg" data-z-index="-1" id="why_we_are">

			<div class="container">
				<div class="row">
					<div class="section2_div_h2_p">
						<h2 class="section2_h2">Какая высота подходит тебе?</h2>
						<p class="section2_p_right">
							Выбери нужную высоту используя ползунок слева
							<i class="fa fa-hand-o-left" aria-hidden="true"></i>
						</p>
					</div>

					<p class="section2_p">
						<label for="amount">Высота:</label>
						<input type="text" id="amount" readonly style="background-color: transparent; margin:0; padding:0; border: none; font-weight:bold; color: #fff;width:30px;">
						<label for="amount">м</label>
					</p>

					<div id="slider-vertical"></div>
					<div class="text_first hidden">
						<span class="span_klenki">Клёнковский мост</span><br><br><br><br>
						<p class="">
							Прыжки с Клёнковского моста могут осуществляться разными способами. Наиболее популярными являются прыжки с верхней фермы, с нижней фермы и троллей.<br><br>
							Каждый год Клёнковский мост собирает любителей Rope Jump на ежегодный фестиваль прыжков.<br><br>
							Также Клёнковский мост является наиболее популярным местом для прыжков со страховкой в Гомеле. За порцией адреналина можно приезжать сюда хоть каждые выходные!

						</p>
					</div>
					<div class="text_second show">
						<br>
						<span class="span_klenki">Труба в д.Василевичи</span><br><br><br><br>
						<p class="">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<br><br>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

						</p>
					</div>
				</div>
			</div>
	</div>
	</header>
	<aside class="between">

	</aside>
<header class="main_head main_color_bg"  data-parallax="scroll" data-image-src="img/feedbackimg.jpg" data-z-index="-1" id="our_places">

	<div class="container">
		<div class="row">
			<div class="feedback_form">
				<form id="callback" class="pop_form">
					<p><b>Заказать обратный звонок</b></p>
					<input class="form-control" type="text" name="name" placeholder="Ваше имя" required>
					<input class="form-control" type="text" name="phone" placeholder="Ваш телефон" required>
					<button class="button" type="submit">Заказать</button>
				</form>
			</div>
			<div class="social_footer">
				<a href="#"><i class="fa fa-vk" aria-hidden="true"></i></a>
				<a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
				<a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
				<a href="#"><i class="fa fa-google" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
</header>
<div id="feed_back" class="last_block">

</div>

	<!-- <section class="section_3"></section> -->




	<!--[if lt IE 9]>
	<script src="libs/html5shiv/es5-shim.min.js"></script>
	<script src="libs/html5shiv/html5shiv.min.js"></script>
	<script src="libs/html5shiv/html5shiv-printshiv.min.js"></script>
	<script src="libs/respond/respond.min.js"></script>
	<![endif]-->
	<script src="libs/jquery/jquery-1.11.1.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="libs/jquery-mousewheel/jquery.mousewheel.min.js"></script>
	<script src="js/common.js"></script>
	<script src="libs/fancybox/jquery.fancybox.pack.js"></script>
	<script src="libs/waypoints/waypoints-1.6.2.min.js"></script>
	<script src="libs/scrollto/jquery.scrollTo.min.js"></script>
	<script src="libs/owl-carousel/owl.carousel.min.js"></script>
	<script src="libs/countdown/jquery.plugin.js"></script>
	<script src="libs/countdown/jquery.countdown.min.js"></script>
	<script src="libs/countdown/jquery.countdown-ru.js"></script>
	<script src="libs/landing-nav/navigation.js"></script>
	<script src="libs/parallax/parallax.min.js"></script>
	<!-- Yandex.Metrika counter -->
	<!-- /Yandex.Metrika counter -->
	<!-- Google Analytics counter -->
	<!-- /Google Analytics counter -->
</body>

</html>
